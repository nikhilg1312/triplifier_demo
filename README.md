**This is a project for demonstrating triplifier as black box for RDF conversion**

steps for setup:
1. download the project from :                    [https://gitlab.com/nikhilg1312/triplifier_demo](https://gitlab.com/nikhilg1312/triplifier_demo)
2. open the project in terminal
3. create volume input_output_a by command:       `docker volume create --name=input_output_a`
4. run the docker compose up : docker-compose up
5. open the browser and open url :                [http://localhost:18080/](http://localhost:18080/)
6. enter username and password both as:           `admin`
6. upload the intended csv or the csv provided in project
7. **ctrl+c** in terminal
8. run the docker compose up :                    `docker-compose up`
9. open the browser and open url :                [http://localhost:17200/](http://localhost:17200/)


Slides available at:
https://docs.google.com/presentation/d/10tIv0S6kPi5BHTJc3RGd5ysPXGk635Ks2ET0J2gRu3w/edit?usp=sharing